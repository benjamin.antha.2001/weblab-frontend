import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../../_services/user/user.service';
import { PostService } from '../../_services/posts/post.service';
import { FormsModule, Validators } from '@angular/forms';
import {ReactiveFormsModule, FormControl, FormGroup } from '@angular/forms';
import { Post, PostState } from '../../models/post.interface';
import { AuthService } from '../../_services/auth/auth.service';
import { MatIconModule } from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { Router } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';

@Component({
  selector: 'app-create-post',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, MatInputModule, MatFormFieldModule, MatIconModule, MatButtonModule, MatCheckboxModule],
  templateUrl: './create-post.component.html',
  styleUrl: './create-post.component.css'
})
export class CreatePostComponent implements OnInit {
  postForm = new FormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    content: new FormControl('', Validators.required),
    place: new FormControl(''),
    isAnEvent: new FormControl(false),
    eventDate: new FormControl(new Date())
  });

  isDraft: boolean = false
  content?: string;
  posts?: any = []
  isAuthorized = false

  constructor(private userService: UserService, private postService: PostService, private router: Router) { }

  ngOnInit(): void {
    console.log("loaded")
    this.userService.getUserData().subscribe({
      next: data => {
        if (data.role === "PUBLISHER"){
          this.content = "You are authorized"
          this.isAuthorized = true
        } else {
          this.content = "Your not authorized";
        }
      },
      error: err => {
        this.isAuthorized = false
        if (err.error) {
          this.content = "Your not authorized"
        } else {
          this.content = "Error with status: " + err.status;
        }
      }
    });
  }

  onSubmit(): void {
    let post: Post = {
      title: this.postForm.value.title ?? '',
      description: this.postForm.value.description ?? '',
      content: this.postForm.value.content ?? '',
      place: this.postForm.value.place ?? '',
      state: this.isDraft ? PostState.Draft : PostState.Published,
      isAnEvent: this.postForm.value.isAnEvent ?? false,
      eventDate: this.postForm.value.eventDate ?? new Date()
    }

    this.postService.createPost(post).subscribe({
      next: data => {
        console.log("Post saved")
        this.goToManagement()
      },
      error: err => {
        console.log(err)
      }
    });
  }

  saveAsDraft(): void {
    this.isDraft = true
    this.onSubmit()
  }

  goToManagement(): void {
    this.router.navigate(['/post-management/']);
  }
}
