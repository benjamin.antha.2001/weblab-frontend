import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user/user.service';
import { PostService } from '../_services/posts/post.service';
import { Router } from '@angular/router';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { DatePipe } from '@angular/common';
import { Post, PostState } from '../models/post.interface';
import { MatIconModule } from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';


@Component({
  selector: 'app-post-management',
  standalone: true,
  imports: [MatTabsModule, MatCardModule, MatButtonModule, DatePipe, MatIconModule, MatDividerModule],
  templateUrl: './post-management.component.html',
  styleUrl: './post-management.component.css'
})
export class PostManagementComponent implements OnInit {
  content?: string;
  posts?: any = []
  isAuthorized = false
  constructor(private router: Router, private userService: UserService, private postService: PostService) { }

  ngOnInit(): void {
    this.getPosts()
  }

  getPosts(): void {
    this.postService.getPostsByPublisher().subscribe({
      next: data => {
        this.posts =JSON.parse(data)
        this.isAuthorized = true
      },
      error: err => {
        this.errorHandling(err, 'posts could not loaded')
      }
    });
  }

  publishDraft(id:string): void {
    let post: any = this.posts.find((post: { _id: string; }) => post._id === id);
    post.state = PostState.Published
    this.postService.updatePost(post).subscribe({
      next: data => {
      },
      error: err => {
        this.errorHandling(err, 'post could not be published')
      }
    });
  }

  deletePost(id:string): void {
    this.postService.deletePost(id).subscribe({
      next: data => {
        const indexOfObject = this.posts.findIndex((post:any) => {
          return post._id === id;
        });
        if (indexOfObject !== -1) {
          this.posts.splice(indexOfObject, 1);
        }
      },
      error: err => {
        this.errorHandling(err, 'post could not be deleted')
      }
    });
  }

  goToCreatePost(): void {
    this.router.navigate(['/post-management/create-post']);
  }

  goToPublishedPost(id: string): void {
    this.router.navigate(['/posts/' + id]);
  }

  goToDraft(id: string): void {
    this.router.navigate(['/drafts/' + id]);
  }

  errorHandling(err: any, message: string): void {
    console.log(err)
    if (err.message == "Session expired") {
      this.router.navigate(['/login']);
    } else if (err.status == 401) {
      this.router.navigate(['/login']);
    } else if (err.status == "failed") {
      this.router.navigate(['/register']);
    } else {
      this.content = message
    }
  }
}
