import { Component, OnInit, Input } from '@angular/core';
import { Post, PostState } from '../../models/post.interface';
import { PostService } from '../../_services/posts/post.service';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatLabel } from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-draft-view',
  standalone: true,
  imports: [MatInputModule, MatFormFieldModule, MatLabel, MatCheckboxModule, MatButtonModule, ReactiveFormsModule, MatIconModule],
  templateUrl: './draft-view.component.html',
  styleUrl: './draft-view.component.css'
})
export class DraftViewComponent implements OnInit{
  editPostForm = new FormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    content: new FormControl('', Validators.required),
    isAnEvent: new FormControl(false),
    place: new FormControl(''),
    eventDate: new FormControl(new Date())
  });

  error?: string;
  postId: string = "";
  @Input()
  set draftId(id: string) {
    this.postId = id
  }
  post?: Post

  constructor(private postService: PostService, private router: Router) { }

  ngOnInit(): void {
    console.log("loaded", this.postId)
    this.postService.getDraftById(this.postId).subscribe({
      next: data => {
        this.post= JSON.parse(data)
        const { title, description, content, isAnEvent, place, eventDate } = JSON.parse(data);

        this.editPostForm.patchValue({
          title,
          description,
          content,
          isAnEvent,
          place,
          eventDate
        })
        console.log(this.editPostForm)
      },
      error: err => {
        if (err.error) {
          this.error = "No Post found"
        } else {
          this.error = "Error with status: " + err.status;
        }
      }
    });
  }

  onSubmit(): void {
    let updatePost: any = {
      title: this.editPostForm.value.title ?? '',
      description: this.editPostForm.value.description ?? '',
      content: this.editPostForm.value.content ?? '',
      isAnEvent: this.editPostForm.value.isAnEvent ?? false,
      place: this.editPostForm.value.place ?? '',
      eventDate: this.editPostForm.value.eventDate ?? new Date()
    }

    this.post = {...this.post,...updatePost}

    if (this.post){
      this.postService.updatePost(this.post).subscribe({
        next: data => {
          console.log("Post updated")
          this.goToManagement()
        },
        error: err => {
          console.log(err)
        }
      });
    }
  }

  updateAndPublishDraft(): void {
    let updatePost: any = {
      title: this.editPostForm.value.title ?? '',
      description: this.editPostForm.value.description ?? '',
      content: this.editPostForm.value.content ?? '',
      isAnEvent: this.editPostForm.value.isAnEvent ?? false,
      state: "PUBLISHED"
    }

    this.post = {...this.post,...updatePost}

    if (this.post) {
      this.postService.updatePost(this.post).subscribe({
        next: data => {
          console.log("Post updated and published")
          this.goToManagement()
        },
        error: err => {
          console.log(err)
        }
      });
    }
  }

  goToManagement(): void {
    this.router.navigate(['/post-management/']);
  }
}
