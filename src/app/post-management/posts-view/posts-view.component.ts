import { Component, Input, OnInit } from '@angular/core';
import { PostService } from '../../_services/posts/post.service';
import { Post } from '../../models/post.interface';
import { DatePipe } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';

@Component({
  selector: 'app-posts-view',
  standalone: true,
  imports: [DatePipe, MatCardModule, MatDividerModule],
  templateUrl: './posts-view.component.html',
  styleUrl: './posts-view.component.css'
})
export class PostsViewComponent implements OnInit {
  error?: string;
  postId: string = "";
  @Input()
  set id(id: string) {
    this.postId = id
  }
  post?: Post

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    console.log("loaded", this.postId)
    this.postService.getPostById(this.postId).subscribe({
      next: data => {
        this.post= JSON.parse(data)
      },
      error: err => {
        if (err.error) {
          this.error = "No Post found"
        } else {
          this.error = "Error with status: " + err.status;
        }
      }
    });
  }
}
