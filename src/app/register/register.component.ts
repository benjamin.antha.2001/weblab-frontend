import { Component } from '@angular/core';
import { AuthService } from '../_services/auth/auth.service';
import {ReactiveFormsModule, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { Roles } from '../models/roles.enum';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [ReactiveFormsModule, MatCheckboxModule, MatButtonModule, MatInputModule, MatCardModule],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  registerForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    role: new FormControl(false)
  });
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService) { }

  onSubmit(): void {
    let userRole: Roles = Roles.Reader
    const username = this.registerForm.value.username ?? ""
    const password = this.registerForm.value.password ?? ""

    if (this.registerForm.value.role) {
      userRole = Roles.Publisher
    }

    this.authService.register(username, password, userRole).subscribe({
      next: data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      error: err => {
        this.errorMessage = "Cannot Register User";
        this.isSignUpFailed = true;
      }
    });
  }
}
