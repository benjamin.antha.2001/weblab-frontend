import { Component, OnInit } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AuthService } from '../_services/auth/auth.service';
import {ReactiveFormsModule, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../_services/user/user.service';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { Roles } from '../models/roles.enum';
@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ MatCardModule, FormsModule, MatFormFieldModule, ReactiveFormsModule, MatButtonModule, MatInputModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{
  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  username = '';

  
  constructor(private userService: UserService, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.userService.getUserData().subscribe({
      next: data => {
        if (data.username != undefined){
          this.isLoggedIn = true
          this.username = data.username;
          if (data.role === Roles.Publisher){
            this.router.navigate(['/post-management']);
          } else {
            this.router.navigate(['/']);
          }
        } else {
          this.isLoggedIn = false
        }
        
      },
      error: err => {
        // console.log(err)
      }
    });
  }

  onSubmit(): void {
    const username = this.loginForm.value.username ?? ''
    const password = this.loginForm.value.password ?? ''

    this.authService.login(username, password).subscribe({
      next: data => {
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.username = data.message.username;
        this.reloadPage()
        if (data.message.role === Roles.Publisher){
          this.router.navigate(['/post-management']);
        } else {
          this.router.navigate(['/']);
        }
      },
      error: err => {
        this.errorMessage = "Falsche Credentials";
        this.isLoginFailed = true;
        console.log("error: haha ", err)
      }
    });
  }

  reloadPage(): void {
    window.location.reload();
  }
}
