import { Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component'; 
import { PostManagementComponent } from './post-management/post-management.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { CreatePostComponent } from './post-management/create-post/create-post.component';
import { PostsViewComponent } from './post-management/posts-view/posts-view.component';
import { DraftViewComponent } from './post-management/draft-view/draft-view.component';

export const routes: Routes = [
    {
        path: '',
        title: 'Home',
        component: HomeComponent,
    },
    {
        path: 'login',
        title: 'Login',
        component: LoginComponent,
    },
    {
        path: 'post-management',
        title: 'Management',
        component: PostManagementComponent,
    },
    {
        path: 'register',
        title: 'Register',
        component: RegisterComponent,
    },
    {
        path: 'profile',
        title: 'Profile',
        component: ProfileComponent,
    },
    {
        path: 'post-management/create-post',
        title: 'Create Post',
        component: CreatePostComponent,
    },
    {
        path: 'posts/:id',
        title: 'View Post',
        component: PostsViewComponent,
    },
    {
        path: 'drafts/:draftId',
        title: 'View Draft',
        component: DraftViewComponent,
    },
];
