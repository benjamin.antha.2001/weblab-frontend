import { Component, OnInit } from '@angular/core';
import { PostService } from '../_services/posts/post.service';
import { Post } from '../models/post.interface';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { DatePipe } from '@angular/common';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import { RouterLink } from '@angular/router';
import {MatChipsModule} from '@angular/material/chips';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [MatTabsModule, MatCardModule, MatButtonModule, DatePipe, MatDividerModule, MatListModule, RouterLink, MatChipsModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {
  posts?: any;
  filterEvents: boolean = true
  filterNews: boolean = true

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.postService.getPublishedPosts().subscribe({
      next: data => {
        this.posts = JSON.parse(data);
      },
      error: err => {console.log(err)
        if (err.error) {
          this.posts = JSON.parse(err.error).message;
        } else {
          this.posts = "Error with status: " + err.status;
        }
      }
    });
  }

  changeFilterNews(event: any): void {
    console.log(this.posts)
    this.filterNews = event.selected
  
  }

  changeFilterEvent(event: any): void {
    this.filterEvents = event.selected
  }
}
