export enum PostState { Published = "PUBLISHED", Draft = "DRAFT"}

export interface Post {
    _id?: string;
    title: string;
    description: string;
    content: string;
    place: string;
    state: PostState;
    isAnEvent: boolean;
    author?: string;
    date?: string
    eventDate?: Date
}