import { Component, OnInit } from '@angular/core';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import { RouterLink, RouterOutlet } from '@angular/router';
import { AuthService } from '../_services/auth/auth.service';
import { UserService } from '../_services/user/user.service';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [RouterLink, RouterOutlet, MatToolbarModule, MatButtonModule, MatIconModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit {
  private role: string = "READER"
  isLoggedIn = false;
  showAdminBoard = false;
  username?: string;

  constructor(private userService: UserService, private authService: AuthService) { }
  
  ngOnInit(): void {
    this.userService.getUserData().subscribe({
      next: data => {
        if (data.role != undefined){
          this.role = data.role;
          this.isLoggedIn = true

          if (this.role === "PUBLISHER"){
            this.showAdminBoard = true
          }
          this.username = data.username;
        } else {
          this.isLoggedIn = false
        }
        
      },
      error: err => {
        // console.log(err)
      }
    });
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: res => {
        window.location.reload();
      },
      error: err => {
        console.log(err);
      }
    });
  }
}
