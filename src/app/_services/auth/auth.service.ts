import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'https://weblab-backend.onrender.com/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<any> {
    return this.http.post(
      AUTH_API + 'login',
      {
        username,
        password,
      },
      httpOptions
    );
  }

  register(username: string, password: string, role: string): Observable<any> {
    return this.http.post(
      AUTH_API + 'register',
      {
        username,
        password,
        role
      },
      httpOptions
    );
  }

  logout(): Observable<any> {
    return this.http.get(AUTH_API + 'logout');
  }
}