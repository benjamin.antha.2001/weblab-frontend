import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../../models/post.interface';

const API_URL = 'https://weblab-backend.onrender.com/post/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) {}

  getPublishedPosts(): Observable<any> {
    return this.http.get(API_URL + 'published', { responseType: 'text' });
  }

  getPostsByPublisher(): Observable<any> {
    return this.http.get(API_URL, { responseType: 'text' });
  }

  getPostById(id: string): Observable<any> {
    return this.http.get(API_URL + id, { responseType: 'text' });
  }

  getDraftById(id: string): Observable<any> {
    return this.http.get(API_URL + 'draft/' +  id, { responseType: 'text' });
  }

  updatePost(
    post: Post
  ): Observable<any> {
    return this.http.patch(
      API_URL + post._id,
      post,
      httpOptions
    );
  }
  
  createPost(
    post: Post
  ): Observable<any> {
    return this.http.post(
      API_URL,
      post,
      httpOptions
    );
  }

  deletePost(id: string): Observable<any> {
    return this.http.delete(API_URL + id, { responseType: 'text' });
  }
}
