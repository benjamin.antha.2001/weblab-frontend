import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpInterceptorFn, HttpHandler, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs';

export const HttpRequestInterceptor: HttpInterceptorFn = (req, next) => {
  req = req.clone({
    withCredentials: true
  });
  
  return next(req);
}
